#!/usr/bin/env python3

import socket
from robot import Robot

PAGE_NOT_FOUND = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <p>Resource not found: {resource}.</p>
  </body>
</html>
"""

class contentApp:

    def parse(self, request):
        """Parse the received request, extracting the relevant information."""

        print("Parse: Parsing")
        return request.split(' ',2)[1]

    def process(self, parsedRequest):

        if parsedRequest not in self.dict.keys():
            print("Process: Returning 404 Resource Not Found")
            return "404 Resource Not Found", PAGE_NOT_FOUND.format(resource=parsedRequest)
        else:
            print("Process: Returning 200 OK")
            self.dict[parsedRequest].retrieve()
            return "200 OK", ("<html><body><p>" + self.dict[parsedRequest].content + "</p></body></html>")

    def __init__(self, hostname, port):

        self.dict = {'/www.wikipedia.org': Robot('https://www.wikipedia.org'),
                     '/www.facebook.com': Robot('https://www.facebook.com'),
                     '/www.youtube.com': Robot('https://www.youtube.com')}
        # Create a TCP objet socket and bind it to a port
        mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        mySocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        mySocket.bind((hostname, port))

        # Queue a maximum of 5 TCP connection requests
        mySocket.listen(5)

        # Accept connections, read incoming data, and call
        # parse and process methods (in a loop)

        try:
            while True:
                print("Waiting for connections")
                (recvSocket, address) = mySocket.accept()
                print("HTTP request received (going to parse and process):")
                request = recvSocket.recv(2048)
                print(request)
                parsedRequest = self.parse(request.decode('utf8'))
                print(parsedRequest)
                (returnCode, htmlAnswer) = self.process(parsedRequest)
                print("Answering back...")
                response = "HTTP/1.1 " + returnCode + " \r\n\r\n" \
                        + htmlAnswer + "\r\n"
                recvSocket.send(response.encode('utf8'))
                recvSocket.close()
        except KeyboardInterrupt:
            print("Closing binded socket")
            mySocket.close()

if __name__ == "__main__":
    testContApp = contentApp("localhost", 1234)