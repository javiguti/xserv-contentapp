import urllib.request

class Robot:
    def __init__(self, url):
        self.url = urllib.request.urlopen(url)
        self.retrieved = False
        self.content = None

    def retrieve(self):
        if self.retrieved:
            print('GUARDADA EN LA CACHÉ\n')
        else:
            print('DESCARGANDO: ' + str(self.url.getcode()) + '\n')
            self.content = self.url.read().decode('utf-8')
            self.retrieved = True

    def show(self):
        if not (self.content is None):
            print(self.content + '\n')
        else:
            print('No se ha descargado la página todavía.\n')
            self.retrieve()
            print(self.content)